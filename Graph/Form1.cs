﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graph
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			Engine.display = pictureBox1;
			load(@"..\..\..\data.txt");
			Engine.InitializeGraphics();
			Engine.RefreshGraph();
		}
		public bool isGraph = false;

		#region Load

		public Edge[] edges;
		public int countEdges;
		public int index = 0;
		void load(string filename)
		{
			TextReader dataload = new StreamReader(filename);
			Engine.n = int.Parse(dataload.ReadLine());
			Engine.matrix = new int[Engine.n, Engine.n];
			Engine.costMatrix = new int[Engine.n, Engine.n];
			Engine.booleanArr = new bool[Engine.n];
			countEdges = File.ReadAllLines(filename).Count() - 1;
			edges = new Edge[countEdges];
			string buffer;
			while ((buffer = dataload.ReadLine()) != null)
			{
				int firstNode = int.Parse(buffer.Split(' ')[0]);
				int lastNode = int.Parse(buffer.Split(' ')[1]);
				int size = int.Parse(buffer.Split(' ')[2]);
				Engine.matrix[firstNode - 1, lastNode - 1] = 1;
				Engine.matrix[lastNode - 1, firstNode - 1] = 1;
				Engine.costMatrix[firstNode - 1, lastNode - 1] = size;
				Engine.costMatrix[lastNode - 1, firstNode - 1] = size;
				edges[index] = new Edge(firstNode, lastNode, size);
				index++;
			}
		}

		#endregion

		#region BFS

		string BFS(int firstNode)
		{
			string result = "";
			Queue A = new Queue();
			for (int i = 0; i < Engine.n; i++)
			{
				Engine.booleanArr[i] = false;
			}
			Engine.booleanArr[firstNode] = true;
			A.add(firstNode);
			while (A.values.Length != 0)
			{
				int temp = A.del();
				result += $"{temp + 1} ";
				for (int i = 0; i < Engine.n; i++)
				{
					if (Engine.matrix[temp, i] != 0 && !Engine.booleanArr[i])
					{
						A.add(i);
						Engine.booleanArr[i] = true;
					}
				}
			}
			return result;
		}

		#endregion

		#region DFS

		string DFS_utils(int firstNode)
		{
			string result = "";
			result += $"{firstNode + 1} ";
			for (int i = 0; i < Engine.n; i++)
				if (Engine.matrix[firstNode, i] != 0 && !Engine.booleanArr[i])
				{
					Engine.booleanArr[i] = true;
					result += DFS_utils(i);
				}
			return result;
		}
		string DFS(int firstNode)
		{
			for (int i = 0; i < Engine.n; i++)
			{
				Engine.booleanArr[i] = false;
			}
			Engine.booleanArr[firstNode] = true;
			return DFS_utils(firstNode);
		}

		#endregion

		#region Eulerian

		public static bool Eulerian()
		{
			int[] grade = new int[Engine.n];
			for (int i = 0; i < Engine.n; i++)
			{
				grade[i] = 0;
				for (int j = 0; j < Engine.n; j++)
				{
					if (Engine.matrix[i, j] == 1)
					{
						grade[i]++;
					}
				}
			}
			int aux = 0;
			for (int i = 0; i < Engine.n; i++)
			{
				if (grade[i] % 2 == 1)
				{
					aux++;
				}
			}
			if (aux > 2)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		#endregion

		#region Cycles

		public static bool isCycleUtil(int[,] temp, int current, bool[] visited, int parent)
		{
			visited[current] = true;
			for (int i = 0; i < Engine.n; i++)
			{
				if (temp[current, i] != 0)
				{
					if (!visited[i])
					{
						if (isCycleUtil(temp, i, visited, current))
						{
							return true;
						}
					}
					else if (i != parent)
					{
						return true;
					}
				}
			}
			return false;
		}
		public static bool isCycle(int[,] tempMatrix)
		{
			bool[] visited = new bool[Engine.n];
			for (int i = 0; i < Engine.n; i++)
			{
				visited[i] = false;
			}
			for (int i = 0; i < Engine.n; i++)
			{
				if (!visited[i])
				{
					if (isCycleUtil(tempMatrix, i, visited, -1))
					{
						return true;
					}
				}
			}
			return false;
		}

		#endregion

		#region Bipartite

		bool isBipartite(int[,] matrix, int start)
		{
			int[] colorArr = new int[Engine.n];
			for (int i = 0; i < Engine.n; ++i)
			{
				colorArr[i] = -1;
			}
			colorArr[start] = 1;
			List<int> q = new List<int>();
			q.Add(start);
			while (q.Count != 0)
			{
				int u = q[0];
				q.RemoveAt(0);
				if (matrix[u, u] == 1)
				{
					return false;
				}
				for (int v = 0; v < Engine.n; ++v)
				{
					if (matrix[u, v] == 1 && colorArr[v] == -1)
					{
						colorArr[v] = 1 - colorArr[u];
						q.Add(v);
					}
					else if (matrix[u, v] == 1 && colorArr[v] == colorArr[u])
					{
						return false;
					}
				}
			}
			return true;
		}

		#endregion

		#region Strongly Connected

		bool isStronglyConnected()
		{
			bool[] ok = new bool[Engine.n];
			string dfs_val = DFS(0);
			string[] arr = dfs_val.Trim().Split(' ');
			for (int i = 0; i < arr.Length; i++)
			{
				int point = int.Parse(arr[i]);
				ok[point - 1] = true;
			}
			for (int i = 0; i < ok.Length; i++)
			{
				if (ok[i] == false)
				{
					return false;
				}
			}
			return true;
		}

		#endregion

		#region Dijkstra
		int minDistance(int[] dist, bool[] sptSet)
		{
			int min = int.MaxValue, min_index = -1;
			for (int i = 0; i < Engine.n; i++)
				if (sptSet[i] == false && dist[i] <= min)
				{
					min = dist[i];
					min_index = i;
				}
			return min_index;
		}

		int[] Dijkstra(int[,] matrix, int start)
		{
			int[] dist = new int[Engine.n];
			bool[] sptSet = new bool[Engine.n];

			for (int i = 0; i < Engine.n; i++)
			{
				dist[i] = int.MaxValue;
				sptSet[i] = false;
			}

			dist[start] = 0;

			for (int count = 0; count < Engine.n - 1; count++)
			{

				int min = minDistance(dist, sptSet);
				sptSet[min] = true;

				for (int i = 0; i < Engine.n; i++)

					if (!sptSet[i] && matrix[min, i] != 0 && dist[min] != int.MaxValue && dist[min] + matrix[min, i] < dist[i])
					{
						dist[i] = dist[min] + matrix[min, i];
					}
			}
			return dist;
		}

		#endregion

		private void button1_Click(object sender, EventArgs e) //Load
		{
			isGraph = true;
			Engine.DrawPoints();
			Engine.DrawEdges();
			Engine.RefreshGraph();
		}

		private void button2_Click(object sender, EventArgs e) //BFS
		{
			label1.Visible = true;
			label1.Text = BFS(0);
		}

		private void button3_Click(object sender, EventArgs e) //DFS
		{
			label2.Visible = true;
			label2.Text = DFS(0);
		}

		private void button4_Click(object sender, EventArgs e) //Paint
		{
			if (isGraph)
			{
				Engine.DrawPoints(true);
				Engine.RefreshGraph();
			}
		}

		private void button5_Click(object sender, EventArgs e) //Eulerian
		{
			label3.Visible = true;
			if (Eulerian())
			{
				label3.Text = "True";
			}
			else
			{
				label3.Text = "False";
			}
		}

		private void button6_Click(object sender, EventArgs e) //Cycles
		{
			label4.Visible = true;
			if (isCycle(Engine.matrix))
			{
				label4.Text = "True";
			}
			else
			{
				label4.Text = "False";
			}
		}

		private void button7_Click(object sender, EventArgs e) //Bipartite
		{
			label6.Visible = true;
			if (isBipartite(Engine.matrix, 0))
			{
				label6.Text = "True";
			}
			else
			{
				label6.Text = "False";
			}
		}

		private void button9_Click(object sender, EventArgs e) //Dijkstra
		{
			label8.Text = "";
			label8.Visible = true;
			int[] intArr = Dijkstra(Engine.costMatrix, 0);
			for (int i = 0; i < intArr.Length; i++)
			{
				if (intArr[i] != int.MaxValue)
				{
					label8.Text += $"{i + 1} {intArr[i]} \n";
				}
				else
				{
					label8.Text += $"{i + 1} ∞ \n";
				}

			}
		}

		private void button12_Click(object sender, EventArgs e) //Connected
		{
			label7.Visible = true;
			if (isStronglyConnected())
			{
				label7.Text = "True";
			}
			else
			{
				label7.Text = "False";
			}
		}

		#region Design

		public Point mouseLocation;
		private void Form1_MouseDown(object sender, MouseEventArgs e)
		{
			mouseLocation = new Point(-e.X, -e.Y);
		}

		private void Form1_MouseMove(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
			{
				Point mousePose = Control.MousePosition;
				mousePose.Offset(mouseLocation.X, mouseLocation.Y);
				Location = mousePose;
			}
		}

		private void button10_Click(object sender, EventArgs e) //X
		{
			this.Close();
		}

		private void button11_Click(object sender, EventArgs e) //Reset
		{
			label1.Visible = false;
			label2.Visible = false;
			label3.Visible = false;
			label4.Visible = false;
			label6.Visible = false;
			label7.Visible = false;
			label8.Visible = false;
			Engine.ClearGraph();
			Engine.RefreshGraph();
		}

		#endregion
	}
}