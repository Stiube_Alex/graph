﻿using System;

namespace Graph
{
	public class Edge
	{
		public int firstNode;
		public int lastNode;
		public int size;

		public Edge(int firstNode, int lastNode, int size)
		{
			this.firstNode = firstNode;
			this.lastNode = lastNode;
			this.size = size;
		}
	}
}
