﻿using System;
using System.Windows.Forms;
using System.Drawing;

namespace Graph
{
    public static class Engine
    {
        public static int n;
        public static int[,] matrix;
        public static bool[] booleanArr;
        public static Graphics grp;
        public static Bitmap bmp;
        public static int rezx, rezy;
        public static Color backColor = Color.Transparent;
        public static PictureBox display;
        public static Random rnd = new Random();
        public static PointF center;
        public static float radius = 150;
        public static PointF[] points;
        public static int[,] costMatrix;

        public static void InitializeGraphics()
        {
            bmp = new Bitmap(display.Width, display.Height);
            grp = Graphics.FromImage(bmp);
            grp.Clear(backColor);
            center = new PointF(display.Width / 2, display.Height / 2);
            points = new PointF[n];
            float uc = (float)(Math.PI * 2) / (float)n;
            for (int i = 0; i < n; i++)
            {
                float x = center.X + radius * (float)Math.Cos(i * uc);
                float y = center.Y + radius * (float)Math.Sin(i * uc);
                points[i] = new PointF(x, y);
            }
        }
        public static void ClearGraph()
        {
            grp.Clear(backColor);
        }

        public static void RefreshGraph()
        {
            display.Image = bmp;
        }

        public static void DrawPoints(bool coloredPoints = false)
        {
            for (int i = 0; i < n; i++)
            {
                grp.DrawEllipse(new Pen(graphColor(), 3), points[i].X - 7, points[i].Y - 7, 15, 15);
                grp.DrawString((i + 1).ToString(), new Font("Arial", 20, FontStyle.Regular), new SolidBrush(graphColor()), points[i]);
                if (coloredPoints)
                {
                    grp.FillEllipse(new SolidBrush(redColor()), points[i].X - 7, points[i].Y - 7, 15, 15);
                }
                else
                {
                    grp.FillEllipse(new SolidBrush(graphColor()), points[i].X - 7, points[i].Y - 7, 15, 15);
                }
            }
        }
        public static void DrawEdges()
        {
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (matrix[i, j] == 1)
                    {

                        grp.DrawLine(new Pen(graphColor(), 2), points[i], points[j]);

                    }
                }
            }
        }
 
        public static Color randomColor()
        {
            return Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
        }
        public static Color graphColor()
        {
            return Color.FromArgb(225, 225, 225);
        }
        public static Color redColor()
        {
            return Color.FromArgb(230, 0, 0);
        }

    }
}